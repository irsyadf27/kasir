/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import koneksi.db;

/**
 *
 * @author Irsyad
 */
public class dbBarang {
    Connection conn;
    ResultSet rs;
    
    public dbBarang() {
        try {
            conn = (Connection) db.configDB();
        } catch (SQLException ex) {
            Logger.getLogger(dbBarang.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Koneksi Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public Integer insert(model.Barang brg) {
        try {
            String sql = "INSERT INTO barang (kode, nama, harga_beli, harga_jual) VALUES (?, ?, ?, ?)";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, brg.getKode());
            pst.setString(2, brg.getNama());
            pst.setLong(3, brg.getHarga_beli());
            pst.setLong(4, brg.getHarga_jual());
            return pst.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(dbBarang.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Insert Barang Error: " + ex.getMessage(), "Insert Error", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }
    
    public Integer update(model.Barang brg) {
        try {
            String sql = "UPDATE barang SET nama=?, harga_beli=?, harga_jual=? WHERE kode=?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, brg.getNama());
            pst.setLong(2, brg.getHarga_beli());
            pst.setLong(3, brg.getHarga_jual());
            pst.setString(4, brg.getKode());
            return pst.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(dbBarang.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Update Barang Error: " + ex.getMessage(), "Update Error", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }
    
    public Integer delete(model.Barang brg) {
        try {
            String sql = "DELETE FROM barang WHERE kode=?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, brg.getKode());
            return pst.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(dbBarang.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Delete Barang Error: " + ex.getMessage(), "Delete Error", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }
    
    public ArrayList<model.Barang> get() {
        ArrayList<model.Barang> lstBarang = new ArrayList<>();
        try {
            String sql = "SELECT * FROM barang ORDER BY nama ASC";
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                lstBarang.add(
                        new model.Barang(rs.getString("kode"), rs.getString("nama"), rs.getLong("harga_beli"), rs.getLong("harga_jual"))
                );	
            }
            rs.close();
        } catch (Exception ex) {
            Logger.getLogger(dbBarang.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Get Barang Error: " + ex.getMessage(), "Get Error", JOptionPane.ERROR_MESSAGE);
        }
        return lstBarang;
    }
    
    public ArrayList<model.Barang> search(String keyword) {
        ArrayList<model.Barang> lstBarang = new ArrayList<>();
        try {
            String sql = "SELECT * FROM barang WHERE kode LIKE ? OR nama LIKE ? ORDER BY nama ASC";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, keyword + "%");
            pst.setString(2, "%" + keyword + "%");
            ResultSet rs = pst.executeQuery();

            while (rs.next()) { 
                lstBarang.add(
                        new model.Barang(rs.getString("kode"), rs.getString("nama"), rs.getLong("harga_beli"), rs.getLong("harga_jual"))
                );	
            }
            rs.close();
        } catch (Exception ex) {
            Logger.getLogger(dbBarang.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Search Barang Error: " + ex.getMessage(), "Search Error", JOptionPane.ERROR_MESSAGE);
        }
        return lstBarang;
    }
    
    public ArrayList<model.Barang> getByBarcode(String barcode) {
        ArrayList<model.Barang> lstBarang = new ArrayList<>();
        try {
            String sql = "SELECT * FROM barang WHERE kode LIKE ? ORDER BY nama ASC";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, barcode + "%");
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                lstBarang.add(
                        new model.Barang(rs.getString("kode"), rs.getString("nama"), rs.getLong("harga_beli"), rs.getLong("harga_jual"))
                );	
            }
            rs.close();
        } catch (Exception ex) {
            Logger.getLogger(dbBarang.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Search Barang Error: " + ex.getMessage(), "Search Error", JOptionPane.ERROR_MESSAGE);
        }
        return lstBarang;
    }
}
