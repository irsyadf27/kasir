/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import koneksi.db;

/**
 *
 * @author Irsyad
 */
public class dbStrukDetail {
    Connection conn;
    ResultSet rs;
    
    public dbStrukDetail() {
        try {
            conn = (Connection) db.configDB();
        } catch (SQLException ex) {
            Logger.getLogger(dbStrukDetail.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Koneksi Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public Integer insert(model.StrukDetail f) {
        try {
            String sql = "INSERT INTO struk_detail (no_struk, kode_barang, qty, harga, total) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, f.getNo_struk());
            pst.setString(2, f.getKode_barang());
            pst.setInt(3, f.getQty());
            pst.setString(4, Long.toString(f.getHarga()));
            pst.setString(5, Long.toString(f.getTotal()));
            return pst.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(dbStrukDetail.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Insert Struk Detail Error: " + ex.getMessage(), "Insert Error", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }
    
    public Integer delete(model.Struk f) {
        try {
            String sql = "DELETE FROM struk_detail WHERE no_struk=?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, f.getNo());
            return pst.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(dbStrukDetail.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Delete Struk Detail Error: " + ex.getMessage(), "Delete Error", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }
    
    public ArrayList<model.StrukDetail> get(model.Struk f) {
        ArrayList<model.StrukDetail> lstStruk = new ArrayList<>();
        try {
            String sql = "SELECT * FROM struk_detail WHERE no_struk=?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, f.getNo());
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                lstStruk.add(
                    new model.StrukDetail(
                        rs.getInt("no_struk"),
                        rs.getString("kode_barang"),
                        rs.getInt("qty"),
                        rs.getLong("harga"),
                        rs.getLong("total")
                    )
                );
            }
            rs.close();
        } catch (Exception ex) {
            Logger.getLogger(dbStrukDetail.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Get Struk Detail Error: " + ex.getMessage(), "Get Error", JOptionPane.ERROR_MESSAGE);
        }
        return lstStruk;
    }
}
