/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import koneksi.db;

/**
 *
 * @author Irsyad
 */
public class dbPengaturan {
    Connection conn;
    ResultSet rs;
    
    public dbPengaturan() {
        try {
            conn = (Connection) db.configDB();
        } catch (SQLException ex) {
            Logger.getLogger(dbPengaturan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Koneksi Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public Integer update(model.Pengaturan p) {
        try {
            String sql = "UPDATE pengaturan SET nama=?, alamat=?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, p.getNama());
            pst.setString(2, p.getAlamat());
            return pst.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(dbPengaturan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Update Pengaturan Error: " + ex.getMessage(), "Update Error", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }
    
    public model.Pengaturan get() {
        String nama = "-";
        String alamat = "-";

        try {
            String sql = "SELECT * FROM pengaturan";
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                nama = rs.getString("nama");
                alamat = rs.getString("alamat");
            }
            
            rs.close();
        } catch (Exception ex) {
            Logger.getLogger(dbPengaturan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Get Pengaturan Error: " + ex.getMessage(), "Get Error", JOptionPane.ERROR_MESSAGE);
        }
        return new model.Pengaturan(nama, alamat);
    }
}
