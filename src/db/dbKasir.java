/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import koneksi.db;

/**
 *
 * @author Irsyad
 */
public class dbKasir {
    Connection conn;
    ResultSet rs;
    
    public dbKasir() {
        try {
            conn = (Connection) db.configDB();
        } catch (SQLException ex) {
            Logger.getLogger(dbKasir.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Koneksi Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public Integer insert(model.Kasir ksr) {
        try {
            String sql = "INSERT INTO kasir (nama, alamat, username, password, nohp, foto) VALUES (?, ?, ?, MD5(?), ?, ?)";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, ksr.getNama());
            pst.setString(2, ksr.getAlamat());
            pst.setString(3, ksr.getUsername());
            pst.setString(4, ksr.getPassword());
            pst.setString(5, ksr.getNohp());
            pst.setString(6, ksr.getFoto());
            return pst.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(dbKasir.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Insert Kasir Error: " + ex.getMessage(), "Insert Error", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }
    
    public Integer update(model.Kasir ksr) {
        try {
            String sql = "UPDATE kasir SET nama=:nama, alamat=:alamat, username=:username, nohp=:nohp ";
            if(ksr.getFoto() != null) {
                sql += ", foto=:foto";
            }
            if(ksr.getPassword() != null) {
                sql += ", password=MD5(:password)";
            }
            sql += " WHERE id=:id";
            NamedParameterStatement pst = new NamedParameterStatement(conn, sql);
            pst.setString("nama", ksr.getNama());
            pst.setString("alamat", ksr.getAlamat());
            pst.setString("username", ksr.getUsername());
            pst.setString("nohp", ksr.getNohp());
            if(ksr.getFoto() != null) {
                pst.setString("foto", ksr.getFoto());
            }
            if(ksr.getPassword() != null) {
                pst.setString("password", ksr.getPassword());
            }
            pst.setInt("id", ksr.getId());
            return pst.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(dbKasir.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Update Kasir Error: " + ex.getMessage(), "Update Error", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }
    
    public Integer delete(model.Kasir ksr) {
        try {
            String sql = "DELETE FROM kasir WHERE id=?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, ksr.getId());
            return pst.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(dbKasir.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Delete Kasir Error: " + ex.getMessage(), "Delete Error", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }
    
    public ArrayList<model.Kasir> get() {
        ArrayList<model.Kasir> lstKasir = new ArrayList<>();
        try {
            String sql = "SELECT * FROM kasir ORDER BY nama ASC";
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                lstKasir.add(
                    new model.Kasir(
                        rs.getInt("id"),
                        rs.getString("nama"),
                        rs.getString("alamat"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("nohp"),
                        rs.getString("foto")
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            Logger.getLogger(dbKasir.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Get Kasir Error: " + ex.getMessage(), "Get Error", JOptionPane.ERROR_MESSAGE);
        }
        return lstKasir;
    }
    
    public ArrayList<model.Kasir> search(String keyword) {
        ArrayList<model.Kasir> lstKasir = new ArrayList<>();
        try {
            String sql = "SELECT * FROM kasir WHERE nama LIKE ? OR username LIKE ? ORDER BY nama ASC";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, keyword + "%");
            pst.setString(2, "%" + keyword + "%");
            ResultSet rs = pst.executeQuery();

            while (rs.next()) { 
                lstKasir.add(
                    new model.Kasir(
                        rs.getInt("id"),
                        rs.getString("nama"),
                        rs.getString("alamat"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("nohp"),
                        rs.getString("foto"),
                        new ImageIcon("Images/" + rs.getString("foto"))
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            Logger.getLogger(dbKasir.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Search Kasir Error: " + ex.getMessage(), "Search Error", JOptionPane.ERROR_MESSAGE);
        }
        return lstKasir;
    }
}
