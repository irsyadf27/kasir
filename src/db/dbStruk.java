/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import koneksi.db;

/**
 *
 * @author Irsyad
 */
public class dbStruk {
    Connection conn;
    ResultSet rs;
    
    public dbStruk() {
        try {
            conn = (Connection) db.configDB();
        } catch (SQLException ex) {
            Logger.getLogger(dbStruk.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Koneksi Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public Integer insert(model.Struk f) {
        int last_inserted_id = 0;
        try {
            String sql = "INSERT INTO struk (kasir_id, total, tunai, kembali, total_item, tanggal) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement pst = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pst.setInt(1, f.getKasir_id());
            pst.setString(2, Long.toString(f.getTotal()));
            pst.setString(3, Long.toString(f.getTunai()));
            pst.setString(4, Long.toString(f.getKembali()));
            pst.setInt(5, f.getTotal_item());
            pst.setTimestamp(6, f.getTanggal());
            pst.executeUpdate();
            ResultSet rs = pst.getGeneratedKeys();
            if(rs.next()) {
                last_inserted_id = rs.getInt(1);
            }
        } catch (Exception ex) {
            Logger.getLogger(dbStruk.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Insert Struk Error: " + ex.getMessage(), "Insert Error", JOptionPane.ERROR_MESSAGE);
        }
        return last_inserted_id;
    }
    
    public Integer delete(model.Struk f) {
        try {
            String sql = "DELETE FROM struk WHERE no=?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, f.getNo());
            return pst.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(dbStruk.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Delete Struk Error: " + ex.getMessage(), "Delete Error", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }
    
    public ArrayList<model.Struk> get() {
        ArrayList<model.Struk> lstStruk = new ArrayList<>();
        try {
            String sql = "SELECT * FROM struk ORDER BY tanggal DESC";
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                lstStruk.add(
                    new model.Struk(
                        rs.getInt("no"),
                        rs.getInt("kasir_id"),
                        rs.getLong("total"),
                        rs.getLong("tunai"),
                        rs.getLong("kembali"),
                        rs.getInt("total_item"),
                        rs.getTimestamp("tanggal")
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            Logger.getLogger(dbStruk.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Get Struk Error: " + ex.getMessage(), "Get Error", JOptionPane.ERROR_MESSAGE);
        }
        return lstStruk;
    }
}
