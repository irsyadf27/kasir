/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author Irsyad
 */
public class Struk {
    private Integer no;
    private Integer kasir_id;
    private long total;
    private long tunai;
    private long kembali;
    private Integer total_item;
    private Timestamp tanggal;

    public Struk(Integer no, Integer kasir_id, long total, long tunai, long kembali, Integer total_item, Timestamp tanggal) {
        this.no = no;
        this.kasir_id = kasir_id;
        this.total = total;
        this.tunai = tunai;
        this.kembali = kembali;
        this.total_item = total_item;
        this.tanggal = tanggal;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public Integer getKasir_id() {
        return kasir_id;
    }

    public void setKasir_id(Integer kasir_id) {
        this.kasir_id = kasir_id;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getTunai() {
        return tunai;
    }

    public void setTunai(long tunai) {
        this.tunai = tunai;
    }

    public long getKembali() {
        return kembali;
    }

    public void setKembali(long kembali) {
        this.kembali = kembali;
    }

    public Integer getTotal_item() {
        return total_item;
    }

    public void setTotal_item(Integer total_item) {
        this.total_item = total_item;
    }

    public Timestamp getTanggal() {
        return tanggal;
    }

    public void setTanggal(Timestamp tanggal) {
        this.tanggal = tanggal;
    }
}
