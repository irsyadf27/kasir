/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;
import model.Kasir;

/**
 *
 * @author Irsyad
 */

public class KasirTableModel extends AbstractTableModel {
        private ArrayList<Kasir> data;
        
        private final String[] namaField = {"ID", "Nama", "Username", "No. HP", "Alamat", "Foto"};
        
	public void setData(ArrayList<Kasir> data){
		this.data=data;
	}
        
        @Override
	public int getColumnCount() {
		return namaField.length;
	}
        
        @Override
	public int getRowCount() {
		return data.size();
	}
        
        @Override
	public String getColumnName(int column) {
		return namaField[column];
	}
        
        @Override
	public Object getValueAt(int baris, int kolom) {
		Kasir p = data.get(baris);
		switch(kolom){
			case 0:return p.getId();
			case 1:return p.getNama();
			case 2:return p.getUsername();
                        case 3:return p.getNohp();
                        case 4:return p.getAlamat();
                        case 5:return p.getFoto();
			default:return null;
		}
	}
}
