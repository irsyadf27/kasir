/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Irsyad
 */
public class Barang {
    private String kode;
    private String nama;
    private long harga_beli;
    private long harga_jual;
    
    public Barang(String kode, String nama, String harga_beli, String harga_jual) {
        this.kode = kode;
        this.nama = nama;
        this.harga_beli = Long.parseLong(harga_beli);
        this.harga_jual = Long.parseLong(harga_jual);
    }
    
    public Barang(String kode, String nama, long harga_beli, long harga_jual) {
        this.kode = kode;
        this.nama = nama;
        this.harga_beli = harga_beli;
        this.harga_jual = harga_jual;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public long getHarga_beli() {
        return harga_beli;
    }

    public void setHarga_beli(long harga_beli) {
        this.harga_beli = harga_beli;
    }

    public long getHarga_jual() {
        return harga_jual;
    }

    public void setHarga_jual(long harga_jual) {
        this.harga_jual = harga_jual;
    }
    
    
}
