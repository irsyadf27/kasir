/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Irsyad
 */
public class StrukDetail {
    private Integer no_struk;
    private String kode_barang;
    private Integer qty;
    private long harga;
    private long total;

    public StrukDetail(Integer no_struk, String kode_barang, Integer qty, long harga, long total) {
        this.no_struk = no_struk;
        this.kode_barang = kode_barang;
        this.qty = qty;
        this.harga = harga;
        this.total = total;
    }

    public Integer getNo_struk() {
        return no_struk;
    }

    public void setNo_struk(Integer no_struk) {
        this.no_struk = no_struk;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public long getHarga() {
        return harga;
    }

    public void setHarga(long harga) {
        this.harga = harga;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
    
}
