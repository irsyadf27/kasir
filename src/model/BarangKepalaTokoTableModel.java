/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.Barang;

/**
 *
 * @author Irsyad
 */

public class BarangKepalaTokoTableModel extends AbstractTableModel {
        private ArrayList<Barang> data;
        
        private final String[] namaField = {"Kode","Nama","Harga Beli", "Harga Jual"};
        
	public void setData(ArrayList<Barang> data){
		this.data=data;
	}
        
        @Override
	public int getColumnCount() {
		return namaField.length;
	}
        
        @Override
	public int getRowCount() {
		return data.size();
	}
        
        @Override
	public String getColumnName(int column) {
		return namaField[column];
	}
        
        @Override
	public Object getValueAt(int baris, int kolom) {
		Barang p = data.get(baris);
		switch(kolom){
			case 0:return p.getKode();
			case 1:return p.getNama();
			case 2:return p.getHarga_beli();
                        case 3:return p.getHarga_jual();
			default:return null;
		}
	}
}
