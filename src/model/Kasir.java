/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.swing.ImageIcon;

/**
 *
 * @author Irsyad
 */
public class Kasir {
    private Integer id;
    private String nama;
    private String alamat;
    private String username;
    private String password;
    private String nohp;
    private String foto;
    private ImageIcon foto_img;
    
    public Kasir(Integer id, String nama, String alamat, String username, String password, String nohp, String foto) {
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
        this.username = username;
        this.password = password;
        this.nohp = nohp;
        this.foto = foto;
    }

    public Kasir(Integer id, String nama, String alamat, String username, String password, String nohp, String foto, ImageIcon foto_img) {
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
        this.username = username;
        this.password = password;
        this.nohp = nohp;
        this.foto = foto;
        this.foto_img = foto_img;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNohp() {
        return nohp;
    }

    public void setNohp(String nohp) {
        this.nohp = nohp;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public ImageIcon getFoto_img() {
        ImageIcon icon = new ImageIcon("Images/" + foto);
        return icon;
    }

    public void setFoto_img(ImageIcon foto_img) {
        this.foto_img = foto_img;
    }

    
}
