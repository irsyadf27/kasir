/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import model.*;

/**
 *
 * @author Irsyad
 */
public class Kasir {
    private static int id;
    private static String nama;
    private static String username;
    private static Boolean login = false;
    
    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Kasir.id = id;
    }

    public static String getNama() {
        return nama;
    }

    public static void setNama(String nama) {
        Kasir.nama = nama;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        Kasir.username = username;
    }

    public static Boolean isLogin() {
        return Kasir.login;
    }
    
    public static void setLogin(Boolean login) {
        Kasir.login = login;
    }
    
    public static void reset() {
        Kasir.login = false;
        Kasir.id = -1;
        Kasir.nama = "";
        Kasir.username = "";
    }
}
