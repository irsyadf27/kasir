/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import model.*;

/**
 *
 * @author Irsyad
 */
public class KepalaToko {
    private static int id;
    private static String nama;
    private static String username;
    private static Boolean login = false;
    
    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        KepalaToko.id = id;
    }

    public static String getNama() {
        return nama;
    }

    public static void setNama(String nama) {
        KepalaToko.nama = nama;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        KepalaToko.username = username;
    }

    public static Boolean isLogin() {
        return KepalaToko.login;
    }
    
    public static void setLogin(Boolean login) {
        KepalaToko.login = login;
    }
    
    public static void reset() {
        KepalaToko.login = false;
        KepalaToko.id = -1;
        KepalaToko.nama = "";
        KepalaToko.username = "";
    }
}
