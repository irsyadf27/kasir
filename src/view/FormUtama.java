/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import laporan.GrafikPenjualan;
import laporan.PendapatanDanKeuntungan;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import view.kasir.FormTransaksi;
import view.kasir.KasirLogin;
import view.kepalatoko.FormKelolaBarang;
import view.kepalatoko.FormKelolaKasir;
import view.kepalatoko.KepalaTokoLogin;
import view.kepalatoko.Pengaturan;

/**
 *
 * @author Irsyad
 */
public class FormUtama extends javax.swing.JFrame {

    /**
     * Creates new form FormUtama
     */
    
    FormTransaksi transaksi = new FormTransaksi();
    FormKelolaBarang kelolaBarang = new FormKelolaBarang();
    KasirLogin kasirLogin = new KasirLogin();
    KepalaTokoLogin kepalaLogin = new KepalaTokoLogin();
    FormKelolaKasir kelolaKasir = new FormKelolaKasir();
    
    GrafikPenjualan grafikPenjualan = new GrafikPenjualan();
    PendapatanDanKeuntungan pendapatanKeuntungan = new PendapatanDanKeuntungan();
    Pengaturan pengaturan = new Pengaturan();
    
    public FormUtama() {
        initComponents();
        
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        this.setMaximizedBounds(env.getMaximumWindowBounds());
        this.setExtendedState(this.getExtendedState() | this.MAXIMIZED_BOTH);
        
        jDesktopPane1.add(pendapatanKeuntungan);
        pendapatanKeuntungan.setVisible(true);
        //centerJIF(pendapatanKeuntungan);
                
        // Set Ditengah
        //Dimension desktopSize = jDesktopPane1.getSize();
        //Dimension jInternalFrameSize = kelolaKasir.getSize();
        //kelolaKasir.setLocation((desktopSize.width - jInternalFrameSize.width)/2, (desktopSize.height- jInternalFrameSize.height)/2);
   
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuTransaksi = new javax.swing.JMenu();
        mnuTambahTransaksi = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mnuLoginKasir = new javax.swing.JMenuItem();
        mnuLogoutKasir = new javax.swing.JMenuItem();
        jMenuKelolaToko = new javax.swing.JMenu();
        mnuKelolaBarang = new javax.swing.JMenuItem();
        mnuKelolaKasir = new javax.swing.JMenuItem();
        mnuLaporan = new javax.swing.JMenu();
        mnuBarangPalingLaris = new javax.swing.JMenuItem();
        mnuGrafikPenjualan = new javax.swing.JMenuItem();
        mnuPendapatanKeuntungan = new javax.swing.JMenuItem();
        mnuPengaturan = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        mnuLoginKepalaToko = new javax.swing.JMenuItem();
        mnuLogoutKepalaToko = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Form Utama");
        setSize(new java.awt.Dimension(600, 400));

        jDesktopPane1.setMinimumSize(new java.awt.Dimension(600, 400));
        jDesktopPane1.setSize(new java.awt.Dimension(600, 400));

        jMenuTransaksi.setText("Kasir");

        mnuTambahTransaksi.setText("Tambah Transaksi");
        mnuTambahTransaksi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuTambahTransaksiActionPerformed(evt);
            }
        });
        jMenuTransaksi.add(mnuTambahTransaksi);
        jMenuTransaksi.add(jSeparator1);

        mnuLoginKasir.setText("Login Kasir");
        mnuLoginKasir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuLoginKasirActionPerformed(evt);
            }
        });
        jMenuTransaksi.add(mnuLoginKasir);

        mnuLogoutKasir.setText("Logout");
        mnuLogoutKasir.setEnabled(false);
        mnuLogoutKasir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuLogoutKasirActionPerformed(evt);
            }
        });
        jMenuTransaksi.add(mnuLogoutKasir);

        jMenuBar1.add(jMenuTransaksi);

        jMenuKelolaToko.setText("Kelola Toko");

        mnuKelolaBarang.setText("Kelola Barang");
        mnuKelolaBarang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuKelolaBarangActionPerformed(evt);
            }
        });
        jMenuKelolaToko.add(mnuKelolaBarang);

        mnuKelolaKasir.setText("Kelola Kasir");
        mnuKelolaKasir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuKelolaKasirActionPerformed(evt);
            }
        });
        jMenuKelolaToko.add(mnuKelolaKasir);

        mnuLaporan.setText("Laporan");

        mnuBarangPalingLaris.setText("Barang Paling Laris");
        mnuBarangPalingLaris.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuBarangPalingLarisActionPerformed(evt);
            }
        });
        mnuLaporan.add(mnuBarangPalingLaris);

        mnuGrafikPenjualan.setText("Grafik Penjualan");
        mnuGrafikPenjualan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuGrafikPenjualanActionPerformed(evt);
            }
        });
        mnuLaporan.add(mnuGrafikPenjualan);

        mnuPendapatanKeuntungan.setText("Pendapatan & Keuntungan");
        mnuPendapatanKeuntungan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuPendapatanKeuntunganActionPerformed(evt);
            }
        });
        mnuLaporan.add(mnuPendapatanKeuntungan);

        jMenuKelolaToko.add(mnuLaporan);

        mnuPengaturan.setText("Pengaturan");
        mnuPengaturan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuPengaturanActionPerformed(evt);
            }
        });
        jMenuKelolaToko.add(mnuPengaturan);
        jMenuKelolaToko.add(jSeparator2);

        mnuLoginKepalaToko.setText("Login Kepala Toko");
        mnuLoginKepalaToko.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuLoginKepalaTokoActionPerformed(evt);
            }
        });
        jMenuKelolaToko.add(mnuLoginKepalaToko);

        mnuLogoutKepalaToko.setText("Logout");
        mnuLogoutKepalaToko.setEnabled(false);
        mnuLogoutKepalaToko.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuLogoutKepalaTokoActionPerformed(evt);
            }
        });
        jMenuKelolaToko.add(mnuLogoutKepalaToko);

        jMenuBar1.add(jMenuKelolaToko);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void mnuKelolaBarangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuKelolaBarangActionPerformed
        // TODO add your handling code here:
        if(session.KepalaToko.isLogin()) {
            if (!kelolaBarang.isVisible() && !kelolaBarang.isShowing()) {
                jDesktopPane1.add(kelolaBarang);
                kelolaBarang.setVisible(true);
                centerJIF(kelolaBarang);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Anda Belum Login Sebagai Kepala Toko", "Pemberitahuan", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_mnuKelolaBarangActionPerformed

    private void mnuKelolaKasirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuKelolaKasirActionPerformed
        // TODO add your handling code here:
        if(session.KepalaToko.isLogin()) {
            if (!kelolaKasir.isVisible() && !kelolaKasir.isShowing()) {
                jDesktopPane1.add(kelolaKasir);
                kelolaKasir.setVisible(true);
                centerJIF(kelolaKasir);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Anda Belum Login Sebagai Kepala Toko", "Pemberitahuan", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_mnuKelolaKasirActionPerformed

    private void mnuTambahTransaksiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuTambahTransaksiActionPerformed
        // TODO add your handling code here:
        if(session.Kasir.isLogin()) {
            if (!transaksi.isVisible() && !transaksi.isShowing()) {
                jDesktopPane1.add(transaksi);
                transaksi.setVisible(true);
                centerJIF(transaksi);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Anda Belum Login Sebagai Kasir", "Pemberitahuan", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_mnuTambahTransaksiActionPerformed

    private void mnuLoginKasirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuLoginKasirActionPerformed
        // TODO add your handling code here:
        if(!kasirLogin.isVisible() && !kasirLogin.isShowing()) {
            jDesktopPane1.add(kasirLogin);
            kasirLogin.setVisible(true);
            centerJIF(kasirLogin);
        }
    }//GEN-LAST:event_mnuLoginKasirActionPerformed

    private void mnuLogoutKasirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuLogoutKasirActionPerformed
        // TODO add your handling code here:
        mnuLoginKasir.setEnabled(true);
        mnuLogoutKasir.setEnabled(false);
        session.Kasir.reset();
        JOptionPane.showMessageDialog(null, "Berhasil Logout Dari Kasir", "Pemberitahuan", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_mnuLogoutKasirActionPerformed

    private void mnuLoginKepalaTokoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuLoginKepalaTokoActionPerformed
        // TODO add your handling code here:
        if(!kepalaLogin.isVisible() && !kepalaLogin.isShowing()) {
            jDesktopPane1.add(kepalaLogin);
            kepalaLogin.setVisible(true);
            centerJIF(kepalaLogin);
        }
    }//GEN-LAST:event_mnuLoginKepalaTokoActionPerformed

    private void mnuLogoutKepalaTokoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuLogoutKepalaTokoActionPerformed
        // TODO add your handling code here:
        mnuLoginKepalaToko.setEnabled(true);
        mnuLoginKepalaToko.setEnabled(false);
        session.KepalaToko.reset();
        JOptionPane.showMessageDialog(null, "Berhasil Logout Dari Kepala Toko", "Pemberitahuan", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_mnuLogoutKepalaTokoActionPerformed

    private void mnuBarangPalingLarisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuBarangPalingLarisActionPerformed
        // TODO add your handling code here:
        try {
            Map<String, Object> param = new HashMap<>();
            File file = new File("src/laporan/BarangPalingBanyakDibeli.jrxml");
            JasperDesign jasperDesign = JRXmlLoader.load(file);
            param.clear();
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, param, koneksi.db.configDB());
            JasperViewer.viewReport(jasperPrint, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_mnuBarangPalingLarisActionPerformed

    private void mnuGrafikPenjualanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuGrafikPenjualanActionPerformed
        // TODO add your handling code here:
        if(session.KepalaToko.isLogin()) {
            if(!grafikPenjualan.isVisible() && !grafikPenjualan.isShowing()) {
                jDesktopPane1.add(grafikPenjualan);
                grafikPenjualan.setVisible(true);
                centerJIF(grafikPenjualan);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Anda Belum Login Sebagai Kepala Toko", "Pemberitahuan", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_mnuGrafikPenjualanActionPerformed

    private void mnuPendapatanKeuntunganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuPendapatanKeuntunganActionPerformed
        // TODO add your handling code here:
        if(session.KepalaToko.isLogin()) {
            if(!pendapatanKeuntungan.isVisible() && !pendapatanKeuntungan.isShowing()) {
                jDesktopPane1.add(pendapatanKeuntungan);
                pendapatanKeuntungan.setVisible(true);
                centerJIF(pendapatanKeuntungan);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Anda Belum Login Sebagai Kepala Toko", "Pemberitahuan", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_mnuPendapatanKeuntunganActionPerformed

    private void mnuPengaturanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuPengaturanActionPerformed
        // TODO add your handling code here:
        if(session.KepalaToko.isLogin()) {
            if(!pengaturan.isVisible() && !pengaturan.isShowing()) {
                jDesktopPane1.add(pengaturan);
                pengaturan.setVisible(true);
                centerJIF(pengaturan);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Anda Belum Login Sebagai Kepala Toko", "Pemberitahuan", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_mnuPengaturanActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormUtama().setVisible(true);
            }
        });
    }
    
    private void centerJIF(JInternalFrame jif) {
        Dimension desktopSize = jDesktopPane1.getSize();
        Dimension jInternalFrameSize = jif.getSize();
        int width = (desktopSize.width - jInternalFrameSize.width) / 2;
        int height = (desktopSize.height - jInternalFrameSize.height) / 2;
        jif.setLocation(width, height);
        jif.setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JDesktopPane jDesktopPane1;
    public static javax.swing.JMenuBar jMenuBar1;
    public static javax.swing.JMenu jMenuKelolaToko;
    public static javax.swing.JMenu jMenuTransaksi;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JMenuItem mnuBarangPalingLaris;
    private javax.swing.JMenuItem mnuGrafikPenjualan;
    private javax.swing.JMenuItem mnuKelolaBarang;
    private javax.swing.JMenuItem mnuKelolaKasir;
    private javax.swing.JMenu mnuLaporan;
    public static javax.swing.JMenuItem mnuLoginKasir;
    public static javax.swing.JMenuItem mnuLoginKepalaToko;
    public static javax.swing.JMenuItem mnuLogoutKasir;
    public static javax.swing.JMenuItem mnuLogoutKepalaToko;
    private javax.swing.JMenuItem mnuPendapatanKeuntungan;
    private javax.swing.JMenuItem mnuPengaturan;
    private javax.swing.JMenuItem mnuTambahTransaksi;
    // End of variables declaration//GEN-END:variables
}
