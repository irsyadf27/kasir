-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 08, 2017 at 01:17 AM
-- Server version: 5.7.18
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `kasir`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode` varchar(50) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga_beli` bigint(20) NOT NULL,
  `harga_jual` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode`, `nama`, `harga_beli`, `harga_jual`) VALUES
('8992222053426', 'GATSBY URBAN COLOGNE ENERGY', 0, 20000),
('8992222054966', 'GATSBY POMPADAUR STYLE STYLEIG POMADE', 12000, 22000),
('8992928061008', 'NATUR NATURAL EXTRACT HAIR TONIC', 0, 30000),
('8999809102782', 'NATUR-E DAILY FACE CREAM', 0, 25000),
('8999999047092', 'REXONA MEN ANTIBACTERIAL DEFENSE', 0, 7500);

-- --------------------------------------------------------

--
-- Table structure for table `kasir`
--

CREATE TABLE `kasir` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nohp` varchar(20) NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kasir`
--

INSERT INTO `kasir` (`id`, `nama`, `alamat`, `username`, `password`, `nohp`, `foto`) VALUES
(2, 'budi', 'test', 'budi', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'budi.png'),
(5, 'test', 'test', 'test', 'MD5(\'testtest\')', '2', 'test.png'),
(7, '123', 'test', '123', 'e10adc3949ba59abbe56e057f20f883e', '123', '123.png'),
(8, 'testt', 'test', 'test', '05a671c66aefea124cc08b76ea6d30bb', '123456789', 'test.png');

-- --------------------------------------------------------

--
-- Table structure for table `kepalatoko`
--

CREATE TABLE `kepalatoko` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nohp` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kepalatoko`
--

INSERT INTO `kepalatoko` (`id`, `nama`, `nohp`, `username`, `password`) VALUES
(1, 'Budi', '0123456789', 'budi', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`nama`, `alamat`) VALUES
('Toko-Tokoan', 'JL TERUS MAJU MUNDUR');

-- --------------------------------------------------------

--
-- Table structure for table `struk`
--

CREATE TABLE `struk` (
  `no` int(11) NOT NULL,
  `kasir_id` int(11) NOT NULL,
  `total` bigint(20) NOT NULL,
  `tunai` bigint(20) NOT NULL,
  `kembali` bigint(20) NOT NULL,
  `total_item` int(5) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `struk`
--

INSERT INTO `struk` (`no`, `kasir_id`, `total`, `tunai`, `kembali`, `total_item`, `tanggal`) VALUES
(1, 2, 42000, 50000, 8000, 2, '2017-06-30 12:46:38'),
(2, 2, 29500, 29500, 0, 2, '2017-06-29 12:47:02'),
(3, 2, 97500, 100000, 2500, 3, '2017-06-28 13:59:26'),
(4, 2, 45000, 50000, 5000, 2, '2017-07-02 07:56:46'),
(5, 2, 37500, 40000, 2500, 2, '2017-07-02 08:49:30');

-- --------------------------------------------------------

--
-- Table structure for table `struk_detail`
--

CREATE TABLE `struk_detail` (
  `no_struk` int(11) NOT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `qty` int(5) NOT NULL,
  `harga` bigint(20) NOT NULL,
  `total` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `struk_detail`
--

INSERT INTO `struk_detail` (`no_struk`, `kode_barang`, `qty`, `harga`, `total`) VALUES
(1, '8992222054966', 1, 22000, 22000),
(1, '8992222053426', 1, 20000, 20000),
(2, '8992222054966', 1, 22000, 22000),
(2, '8999999047092', 1, 7500, 7500),
(3, '8999999047092', 1, 7500, 7500),
(3, '8992928061008', 1, 30000, 30000),
(3, '8992222053426', 3, 20000, 60000),
(4, '8999809102782', 1, 25000, 25000),
(4, '8992222053426', 1, 20000, 20000),
(5, '8992928061008', 1, 30000, 30000),
(5, '8999999047092', 1, 7500, 7500);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `kasir`
--
ALTER TABLE `kasir`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama` (`nama`);

--
-- Indexes for table `kepalatoko`
--
ALTER TABLE `kepalatoko`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `struk`
--
ALTER TABLE `struk`
  ADD PRIMARY KEY (`no`),
  ADD KEY `kasir_id` (`kasir_id`);

--
-- Indexes for table `struk_detail`
--
ALTER TABLE `struk_detail`
  ADD KEY `no_struk` (`no_struk`),
  ADD KEY `kode_barang` (`kode_barang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kasir`
--
ALTER TABLE `kasir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `kepalatoko`
--
ALTER TABLE `kepalatoko`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `struk`
--
ALTER TABLE `struk`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;
